--start the server
mysqld --console

--Creating a new database instance (edureka)

--1. Go to the installation directory of MySQL (get in touch with edureka support for installation etc.)
--2. Open mysql in command prompt. Make sure MySQL server is running.
--3. open mysql client
mysql -u root -p -- when prompted, type the password
show databases; -- It will show all the existing databases.
create database if not exists m; -- creating new database edureka
drop database if exists edureka; -- droping the database 

--Introduce Heidi tool.
--create database using heidi

create table MyFirstTable (id int, name varchar(50), gpa float) -- creating table.
drop table MyFirstTable; -- drops table
show tables; -- shows all tables in database.
desc MyFirstTable; -- gives description of the table.

--continue with Heidi


create table employee (name varchar(30), department varchar(30), salary int)


insert into employee (name, department, salary) values ('naresh', 'java', 100)

select * from employee

insert into employee (name, department, salary) values ('sunil', 'j2ee', 200)

--update 

update employee set salary=150 where name = 'naresh'

--view 
create view employee_view as select name, salary from employee

--alter
alter table employee add age int

select avg(salary) from employee
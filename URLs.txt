MySQL
====
https://www3.ntu.edu.sg/home/ehchua/programming/sql/MySQL_HowTo.html

Types of SQL commands
=====================
http://www.informit.com/articles/article.aspx?p=29583&seqNum=3

oracle vs mysql data types
==========================
https://docs.oracle.com/cd/E12151_01/doc.150/e12155/oracle_mysql_compared.htm#BABGACIF
https://docs.oracle.com/cd/B10501_01/win.920/a97249/ch3.htm
http://akyadav2.blogspot.com/2012/06/data-type-comparison-oracle-and-mysql.html

Heidi tool
==========
http://download.cnet.com/HeidiSQL/3000-2065_4-75451504.html

API
===
https://www.techopedia.com/definition/25133/application-programming-interface-api-java

Types of drivers
================
https://www.javatpoint.com/jdbc-driver

https://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html

Store Procedure and Functions
=============================
http://www.mysqltutorial.org/mysql-functions.aspx
https://stackoverflow.com/questions/3744209/mysql-stored-procedure-vs-function-which-would-i-use-when


http://javaconceptoftheday.com/difference-between-executequery-executeupdate-execute-in-jdbc/

https://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html

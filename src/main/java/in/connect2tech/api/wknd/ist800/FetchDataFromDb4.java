package in.connect2tech.api.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FetchDataFromDb4 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";

		String query = "select e1.name name1, e1.salary salary1 from employee2 e1 inner join employee e on e1.salary=e.salary";

		try {
			Connection conn = DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()){
				String name = rs.getString("NAME1");
				int salary = rs.getInt("SALARY1");
				
				System.out.println("name="+name);
				System.out.println("salary="+salary);
				System.out.println("---------------------------------");
			}

			System.out.println("Fetching done...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

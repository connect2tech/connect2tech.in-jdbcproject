package in.connect2tech.api.wknd.ist800;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertIntoDbProc {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";

		String sql = "{call insert_into_employee(?,?,?)}";

		try {
			Connection conn = DriverManager.getConnection(url, "root", "password");
			CallableStatement callable = conn.prepareCall(sql);
			
			for(int i=0;	i<5;	i++){
				callable.setString(1, "Name"+i);
				callable.setString(2, "Designation"+i);
				callable.setInt(3, 1000+i);
				callable.executeQuery();
			}

			System.out.println("Insertion done...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

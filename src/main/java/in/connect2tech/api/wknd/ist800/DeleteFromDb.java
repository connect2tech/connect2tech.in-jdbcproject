package in.connect2tech.api.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DeleteFromDb {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";

		String query = "delete from employee where salary=300";

		try {
			Connection conn = DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();

			stmt.execute(query);

			System.out.println("Delete done...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

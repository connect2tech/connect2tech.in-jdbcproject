package in.connect2tech.api.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertIntoDb {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/api_automation";

		String query = "insert into employee(name, designation, salary) values ('API automation2', 'QA2', 2000)";

		try {
			Connection conn = DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();

			for (int i = 0; i < 10; i++) {
				stmt.execute(query);
			}

			System.out.println("Insertion done...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

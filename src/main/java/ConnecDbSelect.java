import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnecDbSelect {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/my_db";
		
		//String query="create table employee1(id int, name varchar(100), department varchar(100))";
		String query = "select * from employee1";
		
		try {
			Connection conn =DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();
			ResultSet  rs = stmt.executeQuery(query);
			
			while(rs.next()){
				int i = rs.getInt(1);
				System.out.println(i);
				System.out.println("--------------------");
			}
			
			System.out.println("done....");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


package com.c2t.webinar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FetchDataFromDb {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/Webinar";
		String sql = "select * from employee";
		Connection conn = null;
		Statement stmt;
		
		try {
			conn = DriverManager.getConnection(url, "root", "password");
			stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				int id = rs.getInt(1);
				String name = rs.getString("name");
				int salary = rs.getInt(3);
				
				System.out.println(id);
				System.out.println(name);
				System.out.println(salary);
				
				System.out.println("--------------------------");
				
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("done...");
	
	}
}

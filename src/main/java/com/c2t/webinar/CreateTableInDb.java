package com.c2t.webinar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTableInDb {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/Webinar";
		String sql = "create table employee(id int, name varchar(25), age int, salary int)";
		Connection conn;
		Statement stmt;
		
		try {
			conn = DriverManager.getConnection(url, "root", "password");
			// conn.getAutoCommit();//
			stmt = conn.createStatement();
			stmt.execute(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("table created...");
		
		
		
	}
}

package com.c2t.selenium;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashMap;
import java.util.*;

public class Meta {
	public static void main(String[] args) throws Exception {

		String sql = "select * from employee";

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jan2019", "root", "password");
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);



		ResultSetMetaData rsmd = rs.getMetaData();


		int cols = rsmd.getColumnCount();

		System.out.printf("The query fetched %d columns\n", cols);

		System.out.println("These columns are: ");

		for (int i = 1; i <= cols; i++) {

			String colName = rsmd.getColumnName(i);

			String colType = rsmd.getColumnTypeName(i);

			System.out.println(colName + " of type " + colType);

		}

	
		System.out.println("Insertion done...");


	}
}

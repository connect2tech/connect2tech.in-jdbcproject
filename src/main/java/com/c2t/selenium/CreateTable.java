package com.c2t.selenium;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateTable {
	public static void main(String[] args) throws Exception {
		
		String sql="create table employee (name varchar(30), department varchar(30), salary int)";

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jan2019", "root", "password");
		
		Statement stmt = conn.createStatement();
		stmt.execute(sql);
		
		System.out.println("Table creation done...");

	}
}

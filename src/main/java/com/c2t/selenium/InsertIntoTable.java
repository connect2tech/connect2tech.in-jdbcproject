package com.c2t.selenium;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class InsertIntoTable {
	public static void main(String[] args) throws Exception {

		String sql = "insert into practise_details(measure_id, measure, registry_avg, cms_avg, achieved_perf)  values ('PRIME-41',	'Ultrasound Determination of Pregnancy Location for Pregnant Patients with Abdominal Pain','68.78%',	'70%',	'68.78 %')";

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/figmd", "root", "password");
		Statement stmt = conn.createStatement();
		stmt.execute(sql);

		System.out.println("Insertion done...");

	}
}

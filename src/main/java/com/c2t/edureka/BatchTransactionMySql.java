package com.c2t.edureka;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * It should be logic transaction.
 * Debit from one account and credit to another account.
 */
public class BatchTransactionMySql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		
		// Create connection object/ create connection
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/edureka1", "root", "password");

		Statement stmt = connection.createStatement();
		
		connection.setAutoCommit(false);

		//ATM deposit
		stmt.addBatch("insert into employee(id, name, age, salary) values (1, 'java', 10, 1000)");
		stmt.addBatch("insert into employee(id, name, age, salary) values (2, 'java2', 20, 2000)");
		

		try {
			stmt.executeBatch();
		} catch (Exception e) {
			connection.rollback();
			e.printStackTrace();
		}

		connection.commit();

		connection.close();

	}

}

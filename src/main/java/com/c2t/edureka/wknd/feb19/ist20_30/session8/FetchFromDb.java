package com.c2t.edureka.wknd.feb19.ist20_30.session8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FetchFromDb {
	public static void main(String[] args) {

		try {

			String query = "select * from student";

			String url = "jdbc:mysql://localhost:3306/edureka_feb";
			Connection conn = DriverManager.getConnection(url, "root", "password");
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				int id = rs.getInt("ID");
				String name = rs.getString("NAME");
				String dept = rs.getString("DEPARTMENT");

				System.out.println(id);
				System.out.println(name);
				System.out.println(dept);

				System.out.println("-------------------------");

			}

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());
			System.out.println(e);
		}


	}
}

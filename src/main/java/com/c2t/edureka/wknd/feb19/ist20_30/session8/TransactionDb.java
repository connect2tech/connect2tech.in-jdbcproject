package com.c2t.edureka.wknd.feb19.ist20_30.session8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TransactionDb {
	public static void main(String[] args) throws SQLException {
		
		String atm = "insert into student(id, name, department) values (100,'atm' ,'CS')";
		String backend = "insert into student(id, name, department) values (200,'backend' ,'CS')";

		String url = "jdbc:mysql://localhost:3306/edureka_feb19";
		Connection conn = DriverManager.getConnection(url, "root", "password");
		Statement stmt = conn.createStatement();

		conn.setAutoCommit(false);
		
		try {
			stmt.execute(atm);
			stmt.execute(backend);
		} catch (Exception e) {
			System.out.println(e);
			conn.rollback();
		}
		
		conn.commit();
		conn.close();

	}
}

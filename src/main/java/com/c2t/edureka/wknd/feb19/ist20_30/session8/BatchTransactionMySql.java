package com.c2t.edureka.wknd.feb19.ist20_30.session8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * It should be logic transaction.
 * Debit from one account and credit to another account.
 */
public class BatchTransactionMySql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// Load driver

		// Create connection object/ create connection
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/may2018", "root", "password");

		System.out.println("Connectio !!!");

		Statement stmt = connection.createStatement();

		connection.setAutoCommit(false);

		for (int i = 0; i < 5; i++) {
			
			// ATM deposit
			stmt.addBatch("insert into my_table(id, name, department) values (100,'name','dept')");
			stmt.addBatch("insert into my_table(id, name, department) values (101,'name','dept')");
			stmt.addBatch("insert into my_table1(id, name, department) values (102,'name','dept')");

			try {
				stmt.executeBatch();
			} catch (Exception e) {
				connection.rollback();
				e.printStackTrace();
			}
		}

		connection.commit();

		connection.close();

	}

}

package com.c2t.edureka.wknd.feb19.ist20_30.session8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertInDb {
	public static void main(String[] args) throws SQLException {
		
		String query = "insert into student(id, name, department) values (100,'student1' ,'CS')";

		String url = "jdbc:mysql://localhost:3306/edureka_feb19";
		Connection conn = DriverManager.getConnection(url, "root", "password");
		Statement stmt = conn.createStatement();
		
		stmt.execute(query);
		
		conn.close();

		System.out.println("Insert Done...");
	}
}

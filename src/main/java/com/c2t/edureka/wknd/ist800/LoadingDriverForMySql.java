package com.c2t.edureka.wknd.ist800;

import java.sql.DriverManager;

public class LoadingDriverForMySql {
	public static void main(String[] args) throws Exception {
		String url = "jdbc:mysql://localhost:3306/edureka_dec18";
		DriverManager.getConnection(url, "root", "password");
		System.out.println("Done....");
	}
}

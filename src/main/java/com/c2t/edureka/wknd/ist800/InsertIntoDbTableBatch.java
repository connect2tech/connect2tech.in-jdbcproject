package com.c2t.edureka.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class InsertIntoDbTableBatch {
	public static void main(String[] args) throws Exception {

		String sql0 = "insert into employee(id, name, age ,department) values (100, 'Java', 10,'Training')";
		String sql1 = "insert into employee(id, name, age ,department) values (101, 'Java', 10,'Training')";

		String url = "jdbc:mysql://localhost:3306/edureka_dec18";
		Connection conn = DriverManager.getConnection(url, "root", "password");

		Statement stmt = conn.createStatement();
		conn.setAutoCommit(false);
		
		
		stmt.addBatch(sql0);
		stmt.addBatch(sql1);

		try {
			stmt.executeBatch();
		} catch (Exception e) {
			System.out.println(e);
			conn.rollback();
		}
		
		conn.commit();

		System.out.println("Done....");
	}
}

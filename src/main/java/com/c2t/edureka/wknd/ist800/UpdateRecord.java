package com.c2t.edureka.wknd.ist800;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class UpdateRecord {
	public static void main(String[] args) throws Exception {

		String sql = "update employee set name = 'java j2ee soa' where id=100";
		String url = "jdbc:mysql://localhost:3306/edureka_dec18";
		Connection conn = DriverManager.getConnection(url, "root", "password");

		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sql);

		System.out.println("Done....");
	}
}

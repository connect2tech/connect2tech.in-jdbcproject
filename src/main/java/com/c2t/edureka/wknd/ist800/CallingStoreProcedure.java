package com.c2t.edureka.wknd.ist800;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CallingStoreProcedure {
	public static void main(String[] args) throws Exception {

		String sql = "{call proc_my_emp1(?,?,?,?)}";
		String url = "jdbc:mysql://localhost:3306/edureka_dec18";
		Connection conn = DriverManager.getConnection(url, "root", "password");

		CallableStatement cs = conn.prepareCall(sql);

		for (int i = 0; i < 10; i++) {

			cs.setInt(1, 1000 + i);
			cs.setString(2, "name1000" + i);
			cs.setInt(3, 25 + i);
			cs.setString(4, "dept1000" + i);
			cs.executeQuery();
		}

		System.out.println("Done....");
	}
}

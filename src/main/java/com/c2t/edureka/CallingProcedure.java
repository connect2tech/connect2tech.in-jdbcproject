package com.c2t.edureka;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CallingProcedure {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/edureka1";
		String sql = "update employee set salary = 2000 where id = 2";
		Connection conn = null;
		Statement stmt;

		try {
			conn = DriverManager.getConnection(url, "root", "password");
			CallableStatement cs = conn.prepareCall("{call proc_my_emp1(?,?,?,?)}");

			for(int i=0;i<5;i++){
				cs.setInt(1, i+1);
				cs.setString(2, "name"+i);
				cs.setInt(3, 10+i);
				cs.setInt(4, i+20);
				
				cs.execute();
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Update done...");

		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

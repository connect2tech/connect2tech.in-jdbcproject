package com.c2t.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTableMySQL3 {

	public static void main(String[] args) throws ClassNotFoundException {

		try {
			// Create connection object/ create connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/may2018", "root", "password");
			String sql = "create table my_table1(id int, name varchar(20), department varchar(20))";
			
			Statement stmt = con.createStatement();
			stmt.execute(sql);
			
			System.out.println("table created...");
			
		} catch (SQLException e) {
			System.out.println(e);
		}

	}

}

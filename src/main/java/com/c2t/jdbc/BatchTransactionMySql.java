package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * It should be logic transaction.
 * Debit from one account and credit to another account.
 */
public class BatchTransactionMySql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// Load driver
		Class.forName("com.mysql.jdbc.Driver");
		System.out.println("Done!!!");

		// Create connection object/ create connection
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/may2018", "root", "password");

		System.out.println("Connectio !!!");

		Statement stmt = connection.createStatement();
		
		
		connection.setAutoCommit(false);

		//ATM deposit
		stmt.addBatch("insert into my_table(id, name, department) values (100,'name','dept')");
		stmt.addBatch("insert into my_table(id, name, department) values (101,'name','dept')");
		stmt.addBatch("insert into my_table1(id, name, department) values (102,'name','dept')");
		

		try {
			stmt.executeBatch();
		} catch (Exception e) {
			connection.rollback();
			e.printStackTrace();
		}

		connection.commit();

		connection.close();

	}

}

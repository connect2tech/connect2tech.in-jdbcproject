package com.c2t.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlCon {
	public static void main(String args[]) throws SQLException  {

		String url = "jdbc:mysql://localhost:3306/spring_hibernate";
		DriverManager.getConnection(url, "root", "password");
		System.out.println("Connection establised....");
	}
}

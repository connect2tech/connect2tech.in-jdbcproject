package com.c2t.jdbc;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ProcedureMySql {

	public static void main(String[] args) {
		
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/2018", "root",
					"password");
			
			CallableStatement cs = connection.prepareCall("{call proc1(?,?,?)}");
			
			for(int i=0;i<5;i++){
				cs.setInt(1, i+10);
				cs.setString(2, "name"+i);
				cs.setString(3, "department"+i);
				cs.execute();
				
			}
		} catch (SQLException e) {
			System.out.println("e.getErrorCode()="+e.getErrorCode());
		}

		

		System.out.println("done...");

	}

}

package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertValueInTableMySql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {


		try {
			// Create connection object/ create connection
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/may2018", "root", "password");
			String sql = "insert into my_table1(id, name, department) values (100, 'java' , 'java j2ee')";
			
			Statement stmt = con.createStatement();
			stmt.execute(sql);
			
			System.out.println("insertion done ...");
			
		} catch (SQLException e) {
			System.out.println(e);
		}

	
	}

}

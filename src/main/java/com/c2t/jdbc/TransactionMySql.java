package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * It should be logic transaction.
 * Debit from one account and credit to another account.
 */
public class TransactionMySql {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// Load driver
		System.out.println("Done!!!");

		// Create connection object/ create connection
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/may2018", "root", "password");

		String sql1 = "insert into my_table(id, name, department) values (100,'name','dept')";
		String sql2 = "insert into my_table(id, name, department) values (200,'name2','dept2')";
		
		System.out.println("Connectio !!!");

		Statement stmt = connection.createStatement();
		
		
		//connection.setAutoCommit(false);

		//ATM deposit
		
		//update server

		try {
			stmt.execute(sql1);//ATM deposit
			stmt.execute(sql2);//update the backend server..
		} catch (Exception e) {
			connection.rollback();
			e.printStackTrace();
		}

		//connection.commit();

		connection.close();

	}

}

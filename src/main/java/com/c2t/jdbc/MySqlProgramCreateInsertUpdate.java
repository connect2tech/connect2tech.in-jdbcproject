package com.c2t.jdbc;
import java.sql.*;


public class MySqlProgramCreateInsertUpdate {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");//Load the driver.
		String url = "jdbc:mysql://localhost:3306/jdbc_db1";
		String user = "naresh";
		String password = "password";
		Connection con = DriverManager.getConnection(url, user, password);
		System.out.println("Connection successful...");
		
		Statement stmt = con.createStatement();
		
		System.out.println("created statment...");
		//stmt.execute("create table table_using_jdbc( id varchar(20), name varchar(20), salary int)");
		
		System.out.println("Created table successfully !!!");
		
		//stmt.execute("insert into table_using_jdbc(id, name, salary) value ('id002','jai',200)");
		
		System.out.println("insert successful");
		
		stmt.execute("update edureka_jdbc set amount=500 where id=2");
		
		System.out.println("update done....");
		/*///*stmt.execute("create table first_table(id varchar(20), name varchar(20))");
		//stmt.execute("insert into first_table values('id002','Jai')");
		stmt.execute("update first_table set name='naresh chaurasia' where id='id001'");
		
		System.out.println("execute successful...");
		stmt.close();
		
		System.out.println("stmt closed!!!");*/
	}
}

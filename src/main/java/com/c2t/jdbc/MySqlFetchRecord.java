package com.c2t.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MySqlFetchRecord {

	public static void main(String[] args) throws Exception {
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/may2018", "root", "password");
		System.out.println("Connection successful...");
		
		String sql = "select * from my_table";

		Statement stmt = con.createStatement();

		System.out.println("created statment...");
		
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()){
			int id = rs.getInt(1);
			String name = rs.getString("name");
			String dept = rs.getString("department");
			
			System.out.println("id="+id+ ", name="+name+ " , dept="+dept);
		}
		
		

		System.out.println("execute successful...");
		stmt.close();

		System.out.println("stmt closed!!!");
	}

}
